package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    //usar metodo void quando nao precisa de retorno
    @Test
    public void testarSomadeDoisNumeros() {
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }


    @Test
    public void testarSubDoisNumerosFlu() {
        int resultado = Calculadora.sub(5, 2);
        Assertions.assertEquals(3, resultado);

    }

    @Test
    public void testarMultDoisNumeroInt() {
        int resultado = Calculadora.mult(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarMultDoisNumeroFlu() {
        double resultado = Calculadora.multflu(5, 2);
        Assertions.assertEquals(2.5, resultado);
    }
}
