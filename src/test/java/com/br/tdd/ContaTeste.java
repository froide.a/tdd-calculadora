package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {

    private Cliente cliente;
    private Conta conta;


    @BeforeEach
    public void setUP() {
        this.cliente = new Cliente("Froide");
        this.conta = new Conta(this.cliente, 100.00);
    }


    @Test
    public void testarDepositoEmConta() {
        double valorDeDeposito = 1000.00;
        conta.depositar(valorDeDeposito);

        Assertions.assertEquals(1080.00, conta.getSaldo());
    }

    @Test
    public void testarSacarDeConta() {
        double valorDeSaque = 50.00;
        conta.sacar(valorDeSaque);

        Assertions.assertEquals(50.00, conta.getSaldo());
    }

    //teste de execução com lambda - ver qual a diferença
    @Test
    public void testarSaqueSemSaldoNaConta() {
        double valorDeSaque = 600.00;

        Assertions.assertThrows(RuntimeException.class,/* lambda */() -> {conta.sacar(valorDeSaque);});
    }

}

